#![allow(dead_code)]
extern crate nalgebra as na;
extern crate nalgebra_glm as glm;
use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Transform {
    #[serde(skip)]
    #[serde(default = "glm::Mat4::identity")]
    matrix: glm::Mat4,
    pos: glm::Vec3,
    scale: glm::Vec3,
    ang: glm::Quat,
    #[serde(skip)]
    dirty: bool,
}

impl Transform {
    pub fn new() -> Self {
        Transform {
            matrix: glm::Mat4::identity(),
            pos: glm::Vec3::new(0.0, 0.0, 0.0),
            scale: glm::Vec3::new(1.0, 1.0, 1.0),
            ang: glm::Quat::identity(),
            dirty: true,
        }
    }
    pub fn matrix(&mut self) -> &glm::Mat4 {
        if self.dirty {
            self.matrix = glm::translation(&self.pos) * glm::scaling(&self.scale) * glm::quat_to_mat4(&self.ang);
        }
        &self.matrix
    }
    pub fn set_matrix(&mut self, mat: glm::Mat4) {
        self.dirty = false;
        self.matrix = mat;
    }
    pub fn pos(&self) -> &glm::Vec3 {
        &self.pos
    }
    pub fn set_pos(&mut self, pos: glm::Vec3) -> &mut Self {
        self.pos = pos;
        self.dirty = true;
        return self;
    }
    pub fn ang(&self) -> &glm::Quat {
        &self.ang
    }
    pub fn set_ang(&mut self, ang: glm::Quat) -> &mut Self {
        self.ang = ang;
        self.dirty = true;
        return self;
    }
    pub fn scale(&self) -> &glm::Vec3 {
        &self.scale
    }
    pub fn set_scale(&mut self, scale: glm::Vec3) -> &mut Self{
        self.scale = scale;
        self.dirty = true;
        return self;
    }
}
