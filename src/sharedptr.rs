use std::sync::Arc;
use std::sync::RwLock;
use std::sync::RwLockReadGuard;
use std::sync::RwLockWriteGuard;

#[derive(Clone)]
pub struct SharedPtr<T> {
    ptr: Arc<RwLock<T>>,
}

impl<T> SharedPtr<T> {
    pub fn new(t: T) -> Self {
        SharedPtr {
            ptr: Arc::new(RwLock::new(t)),
        }
    }
    pub fn get(&self) -> RwLockReadGuard<'_, T> {
        self.ptr.read().unwrap()
    }
    pub fn get_mut(&self) -> RwLockWriteGuard<'_, T> {
        self.ptr.write().unwrap()
    }
}
