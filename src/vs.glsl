in vec3 position;
in vec3 normal;

out vec3 v_normal;
out float v_negative;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 transform;
uniform float morph0;
uniform float morph1;
uniform float morph2;
uniform float morph3;

void main() {
	float pi = acos(-1.0);
	v_normal = normal;
	v_negative = 0.0f;
	vec3 pos2 = vec3(0, 0, 0);

	// Spherical coordinates from http://www.theochem.ru.nl/~pwormer/Knowino/knowino.org/wiki/Spherical_polar_coordinates.html
	float r = length(position);
	if (r != 0) {
		float colattitude = acos(position.z/r);

		float azimuth = 0;
		float r_ = r * sin(colattitude);
		if (r_ != 0) {
			float xr = position.x/r_;
			if (xr > 1.0f) {
				xr = 1.0f;
			}
			if (xr < -1.0f) {
				xr = -1.0f;
			}
			azimuth = acos(xr);
			if (position.y < 0.0f) {
				azimuth = 2.0f * pi - azimuth;
			}
		}

		// Do transformation
		float old_r = r;
		r = morph0 * (1.0f / 2.0f) / sqrt(2.0f * pi);
		r += morph1 * (1.0f / 2.0f) * sqrt(3.0f / pi) * sin(colattitude) * sin(azimuth);
		r += morph2 * (1.0f / 2.0f) * sqrt(3.0f / pi) * cos(colattitude);
		r += morph3 * (1.0f / 2.0f) * sqrt(3.0f / pi) * sin(colattitude) * cos(azimuth);
		r = r / ((1.0f / 2.0f) / sqrt(2.0f * pi)); // Normalize radius

		bool rback = false;
		if (r < 0.0f) {
			r *= -1.0f;
			rback = true;
		}

		r *= old_r;
		
		// Convert back to cartesian
		pos2.x = r * sin(colattitude) * cos(azimuth);
		pos2.y = r * sin(colattitude) * sin(azimuth);
		pos2.z = r * cos(colattitude);

		if (rback) {
			v_negative = 1.0f;
		}
	}
	

	gl_Position = projection * view * transform * vec4(pos2, 1.0);
}