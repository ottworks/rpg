use crate::graphics::{Vertex, VertexIndex, VertexNormal, VertexPosition};
use luminance::context::GraphicsContext;
use luminance::tess::{Mode, Tess, TessBuilder};
use std::path::{Path};

pub struct Model {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<VertexIndex>,
    pub tess: Tess,
}

impl Model {
    pub fn load<C>(path: &std::path::PathBuf, surface: &mut C) -> Result<Self, &'static String> where C: GraphicsContext {
        let res = gltf::import(path);
        let (document, buffers, _images) = match res {
            Ok(n) => n,
            Err(err) => panic!("Error loading model {:?}: {}", path, err),
        };
        
        let mut vertices: Vec<Vertex> = Vec::new();
        let mut indices: Vec<VertexIndex> = Vec::new();

        for mesh in document.meshes() {
            println!("Mesh #{}", mesh.index());
            for primitive in mesh.primitives() {
                println!("\tPrimitive #{}", primitive.index());
                let reader = primitive.reader(|buffer| Some(&buffers[buffer.index()]));
                let positions = reader.read_positions().unwrap();
                let mut normals = reader.read_normals().unwrap();
                let v_indices = reader.read_indices().unwrap();

                vertices = positions.map(|position| Vertex {
                    position: VertexPosition::new(position),
                    normal: VertexNormal::new(normals.next().unwrap()),
                }).collect();

                indices = v_indices.into_u32().map(|index| index).collect();
            }
        }
        let tess = TessBuilder::new(surface)
        .set_mode(Mode::Triangle)
        .add_vertices(&vertices)
        .set_indices(&indices)
        .build().unwrap();

        Ok(Model {
            vertices,
            indices,
            tess,
        })
    }
}