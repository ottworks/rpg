use luminance_glfw::{Action, GlfwSurface, Key, Surface as _, WindowDim, WindowEvent, WindowOpt, MouseButton};

use std::process::exit;
use crate::console::Console;
use std::collections::HashMap;
use glfw::CursorMode;

pub struct Window {
    pub surface: GlfwSurface,
    pub text_input: bool,
    pub text: String,
    pub keys_down: HashMap<Key, bool>,
    pub mouse_dx: f32,
    pub mouse_dy: f32,
    pub mouse_x: f32,
    pub mouse_y: f32,
    pub mouse_look: bool,
}

impl Window {
    pub fn new() -> Window {
        let surface = GlfwSurface::new(WindowDim::Windowed(960, 540), "Hello", WindowOpt::default());
        match surface {
            Ok(surface) => {
                eprintln!("yay, created!");
                Window {
                    surface,
                    text_input: false,
                    text: String::new(),
                    keys_down: HashMap::new(),
                    mouse_dx: 0.0,
                    mouse_dy: 0.0,
                    mouse_x: 0.0,
                    mouse_y: 0.0,
                    mouse_look: false,
                }
            }

            Err(e) => {
                eprintln!("cannot create surface!\n{}", e);
                exit(1);
            }
        }
    }
    pub fn input(&mut self, console: &mut Console) -> bool {
        self.mouse_dx = 0.0;
        self.mouse_dy = 0.0;
        for event in self.surface.poll_events() {
            match event {
                WindowEvent::Close | WindowEvent::Key(Key::Escape, _, Action::Release, _) => return false,
                WindowEvent::Key(Key::Enter, _, Action::Press, _) => {
                    if self.text_input {
                        console.parse_exec(self.text.to_string());
                        self.text.clear();
                        self.text_input = false;
                    } else {
                        self.text_input = true;
                    }
                    return true;
                },
                WindowEvent::Char(c) => {
                    if self.text_input {
                        self.text.push(c);
                        println!("> {}", self.text);
                    }
                },
                WindowEvent::Key(key, _, action, _) => {
                    self.keys_down.insert(key, action != Action::Release);
                    
                    if !self.text_input {
                        if console.binds.get().contains_key(&key) {
                            let bind = console.binds.get().get(&key).unwrap().to_string();
                            if bind.starts_with("+") {
                                if action == Action::Press {
                                    console.parse_exec(bind.to_string());
                                } else if action == Action::Release {
                                    console.parse_exec(bind.replace("+", "-"));
                                }
                            } else if action == Action::Press {
                                console.parse_exec(bind.to_string());
                            }
                        }
                    }
                    
                },
                WindowEvent::MouseButton(button, action, _) => {
                    if (button == MouseButton::Button2) {
                        self.mouse_look = action != Action::Release;
                    }
                }
                _ => ()
            }
        }
        if self.surface.window.is_focused() {
            let pos = self.surface.window.get_cursor_pos();

            if (self.mouse_look) {
                let size = self.surface.window.get_size();
                let halfx = size.0 as f64 / 2.0;
                let halfy = size.1 as f64 / 2.0;
                self.mouse_dx = (pos.0 - halfx) as f32;
                self.mouse_dy = (pos.1 - halfy) as f32;
                self.surface.window.set_cursor_mode(CursorMode::Hidden);
                self.surface.window.set_cursor_pos(halfx, halfy);
            } else {
                self.surface.window.set_cursor_mode(CursorMode::Normal);
            }
            
            self.mouse_x = pos.0 as f32;
            self.mouse_y = pos.1 as f32;
        } else {
            self.surface.window.set_cursor_mode(CursorMode::Normal);
        }
        
        return true;
    }
}