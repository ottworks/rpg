#[derive(Copy, Clone)]
pub struct Length {
    pub pixels: i32,
    pub percent: f32,
    pub portion: f32,
}

impl Length {
    fn new() -> Self {
        Self {
            pixels: 0,
            percent: 0.0,
            portion: 0.0,
        }
    }
    fn pixels(pixels: i32) -> Self {
        Self {
            pixels,
            percent: 0.0,
            portion: 0.0,
        }
    }
    fn percent(percent: f32) -> Self {
        Self {
            pixels: 0,
            percent,
            portion: 0.0,
        }
    }
    fn portion(portion: f32) -> Self {
        Self {
            pixels: 0,
            percent: 0.0,
            portion,
        }
    }
}

impl std::ops::Add for Length {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            pixels: self.pixels + other.pixels,
            percent: self.percent + other.percent,
            portion: self.portion + other.portion,
        }
    }
}

impl std::ops::AddAssign for Length {
    fn add_assign(&mut self, other: Self) {
        self.pixels += other.pixels;
        self.percent += other.percent;
        self.portion += other.portion;
    }
}

#[derive(Copy, Clone)]
pub enum UIDirection {
    Row,
    Col,
}

#[derive(Copy, Clone)]
pub struct LRTB {
    pub left: i32,
    pub right: i32,
    pub top: i32,
    pub bottom: i32,
}

impl LRTB {
    pub fn new() -> Self {
        LRTB {
            left: 4,
            right: 4,
            top: 4,
            bottom: 4,
        }
    }
}

pub struct BoxModel {
    margin: LRTB,
    padding: LRTB,
}

impl BoxModel {
    pub fn new() -> Self {
        BoxModel {
            margin: LRTB::new(),
            padding: LRTB::new(),
        }
    }
}

pub struct UINode {
    pub elem: Box<dyn UIElem>,
    pub children: Vec<UINode>,
}

impl UINode {
    pub fn new<E>(elem: E) -> Self where E: UIElem + 'static {
        UINode {
            elem: Box::new(elem),
            children: Vec::new(),
        }
    }
    pub fn clear(&mut self) {
        self.children.clear();
    }
    pub fn walk(&self, depth: usize) -> &Self {
        if depth == 0 {
            return self;
        }
        return self.children.last().unwrap().walk(depth - 1);
    }
    pub fn walk_mut(&mut self, depth: usize) -> &mut Self {
        if depth == 0 {
            return self;
        }
        return self.children.last_mut().unwrap().walk_mut(depth - 1);
    }

}

pub struct UIStack {
    pub tree: UINode,
    pub depth: usize,
    pub rects: Vec<(f32, f32, f32, f32, u32)>,
}

impl UIStack {
    pub fn new() -> Self {
        UIStack {
            tree: UINode::new(Div::new()),
            depth: 0,
            rects: Vec::new(),
        }
    }
    pub fn clear(&mut self) {
        self.tree.clear();
        self.depth = 0;
        self.rects.clear();
    }
    pub fn build<E, F>(&mut self, elem: E, fun: F) where E: UIElem + 'static, F: Fn(&mut UIStack) {
        // This gets called for the outermost element first
        // println!("{:\t<1$}Push!", "", self.depth);
        self.depth += 1;

        self.tree.walk_mut(self.depth - 1).children.push(UINode::new(elem));

        if self.depth >= 2 {
            // Propagate offset down to children
            let parent = self.tree.walk_mut(self.depth - 1);
            let mut new_offset = parent.elem.content_offset();
            
            for i in 0..parent.children.len() - 1 {
                let child_size = parent.children[i].elem.size();
                match parent.elem.layout_direction() {
                    UIDirection::Row => new_offset.0 += child_size.0,
                    UIDirection::Col => new_offset.1 += child_size.1,
                }
            }

            parent.children.last_mut().unwrap().elem.set_offset(new_offset);
        }

        // Recursion
        fun(self);

        // Recursive closure: this gets called for the innermost element first
        if self.depth >= 2 {
            // Propagate size up to parents
            let parent = self.tree.walk_mut(self.depth - 1);

            let child_size = parent.children.last().unwrap().elem.size();
            let mut parent_content_size = parent.elem.content_size();
            match parent.elem.layout_direction() {
                UIDirection::Row => {
                    parent_content_size.0.pixels += child_size.0.pixels;
                    parent_content_size.1.pixels = parent_content_size.1.pixels.max(child_size.1.pixels);
                },
                UIDirection::Col => {
                    parent_content_size.0.pixels = parent_content_size.0.pixels.max(child_size.0.pixels);
                    parent_content_size.1.pixels += child_size.1.pixels;
                },
            }
            parent.elem.set_content_size(parent_content_size);
        }
        
        let offset = self.tree.walk(self.depth).elem.offset();
        let size = self.tree.walk(self.depth).elem.size();
        let color = self.tree.walk(self.depth).elem.background_color();
        let margin = self.tree.walk(self.depth).elem.box_model().margin;
        self.rects.push((
            (offset.0.pixels + margin.left) as f32, 
            (offset.1.pixels + margin.top) as f32, 
            (size.0.pixels - margin.left - margin.right) as f32, 
            (size.1.pixels - margin.top - margin.bottom) as f32,
            color
        ));
        
        self.depth -= 1;
        // println!("{:\t<1$}Pop!", "", self.depth);
        // println!("Children: {}", self.tree.walk(self.depth).children.len());
    }
}


pub trait UIElem {
    fn content_size(&self) -> (Length, Length);
    fn set_content_size(&mut self, size: (Length, Length));
    fn size(&self) -> (Length, Length);
    fn offset(&self) -> (Length, Length);
    fn set_offset(&mut self, offset: (Length, Length));
    fn content_offset(&self) -> (Length, Length);
    fn background_color(&self) -> u32;
    fn box_model(&self) -> &BoxModel;
    fn layout_direction(&self) -> UIDirection;
}

pub struct Div {
    box_model: BoxModel,
    content_size: (Length, Length),
    offset: (Length, Length),
    background_color: u32,
    layout_direction: UIDirection,
    should_build: bool,
}

impl Div {
    pub fn new() -> Self {
        Div {
            box_model: BoxModel::new(),
            content_size: (Length::new(), Length::new()),
            offset: (Length::new(), Length::new()),
            background_color: 0xA0B0C0FF,
            layout_direction: UIDirection::Col,
            should_build: true,
        }
    }
    pub fn new_if(cond: bool) -> Self {
        let mut elem = Self::new();
        elem.should_build = cond;
        return elem;
    }
    pub fn build<F>(self, ui: &mut UIStack, fun: F) where F: Fn(&mut UIStack) {
        if self.should_build {
            ui.build(self, fun);
        }
    }
    pub fn margin(mut self, left: i32, right: i32, top: i32, bottom: i32) -> Self {
        self.box_model.margin.left = left;
        self.box_model.margin.right = right;
        self.box_model.margin.top = top;
        self.box_model.margin.bottom = bottom;
        return self;
    }
    pub fn padding(mut self, left: i32, right: i32, top: i32, bottom: i32) -> Self {
        self.box_model.padding.left = left;
        self.box_model.padding.right = right;
        self.box_model.padding.top = top;
        self.box_model.padding.bottom = bottom;
        return self;
    }
    pub fn background_color(mut self, col: u32) -> Self {
        self.background_color = col;
        return self;
    }
    pub fn layout_direction(mut self, direction: UIDirection) -> Self {
        self.layout_direction = direction;
        return self;
    }
    pub fn width(mut self, width: i32) -> Self {
        self.content_size.0.pixels = width;
        return self;
    }
    pub fn height(mut self, height: i32) -> Self {
        self.content_size.1.pixels = height;
        return self;
    }
}

impl UIElem for Div {
    fn content_size(&self) -> (Length, Length) {
        return self.content_size;
    }
    fn set_content_size(&mut self, size: (Length, Length)) {
        self.content_size = size;
    }
    fn size(&self) -> (Length, Length) {
        let mut size = self.content_size();
        size.0.pixels += self.box_model.margin.left + self.box_model.margin.right;
        size.0.pixels += self.box_model.padding.left + self.box_model.padding.right;
        size.1.pixels += self.box_model.margin.top + self.box_model.margin.bottom;
        size.1.pixels += self.box_model.padding.top + self.box_model.padding.bottom;
        return size;
    }
    fn offset(&self) -> (Length, Length) {
        return self.offset;
    }
    fn set_offset(&mut self, offset: (Length, Length)) {
        self.offset = offset;
    }
    fn content_offset(&self) -> (Length, Length) {
        let mut offset = self.offset();
        offset.0.pixels += self.box_model.margin.left + self.box_model.padding.left;
        offset.1.pixels += self.box_model.margin.top + self.box_model.padding.top;
        return offset;
    }
    fn background_color(&self) -> u32 {
        return self.background_color;
    }
    fn box_model(&self) -> &BoxModel {
        return &self.box_model;
    }
    fn layout_direction(&self) -> UIDirection {
        return self.layout_direction;
    }
}
