mod graphics;
mod window;
mod model;
mod transform;
mod resources;
mod console;
mod sharedptr;
mod camera;
mod physics;
mod charactercontroller;
mod ui;

use graphics::Graphics;
use window::Window;
use hecs::World;
use transform::Transform;
use resources::Resources;
use std::time::Instant;
use console::Console;
use camera::Camera;
use physics::{Physics, PhysicsObj, PhysicsShape, PhysicsStatus};
use serde_json;

use resources::ModelId;
use serde::{Serialize, Deserialize};
use charactercontroller::CharacterController;
use ui::{Div, UIDirection};

extern crate nalgebra as na;
extern crate nalgebra_glm as glm;

use ncollide3d::shape::{ShapeHandle, Ball, Cuboid, TriMesh, ConvexHull};
use ncollide3d::transformation::convex_hull;

use nphysics3d::object::{RigidBodyDesc, ColliderDesc, BodyPartHandle, DefaultBodyHandle};
use nphysics3d::material::{MaterialHandle, BasicMaterial};
use nphysics3d::algebra::Velocity3;

use num_traits::identities::Zero;

struct Player {

}

struct Engine {
    resources: Resources,
    gfx: Graphics,
    window: Window,
    world: World,
    console: Console,
    physics: Physics,
}

fn serialize_entity(entity: hecs::EntityRef<'_>) -> String {
    fn s<T: hecs::Component + Serialize>(entity: hecs::EntityRef<'_>) -> Option<String> {
        if let Some(t) = entity.get::<T>() {
            return serde_json::to_string(&*t).ok();
        }
        None
    }

    const FUNCTIONS: &[&dyn Fn(hecs::EntityRef<'_>) -> Option<String>] = &[
        &s::<Camera>, 
        &s::<Transform>,
        &s::<ModelId>,
        &s::<PhysicsObj>,
    ];

    let mut out = Vec::new();
    for f in FUNCTIONS {
        if let Some(x) = f(entity) {
            out.push(x);
        } else {
            println!("Couldn't serialize entity component");
        }
    }
    serde_json::to_string(&out).unwrap()
}

fn deserialize_entity<'b>(json: &'b str, builder: &mut hecs::EntityBuilder) {
    let components = serde_json::from_str::<Vec<String>>(json);
    
    fn d<'a, T: hecs::Component + serde::de::DeserializeOwned>(string: &'a str, builder: &mut hecs::EntityBuilder) {
       if let Some(t) = serde_json::from_str::<T>(string).ok() {
           builder.add(t);
       }
    }

    const FUNCTIONS: &[&dyn for<'a> Fn(&'a str, &mut hecs::EntityBuilder)] = &[
        &d::<Camera>, 
        &d::<Transform>,
        &d::<ModelId>,
        &d::<PhysicsObj>,
    ];
    if let Some(cs) = components.ok() {
        for c in cs {
            for f in FUNCTIONS {
                f(c.as_str(), builder);
            }
        }
    }
}

fn main() {
    let mut window = Window::new();
    let mut engine = Engine {
        resources: Resources::new(&mut window.surface),
        gfx: Graphics::new(&mut window.surface),
        window,
        world: World::new(),
        console: Console::new(),
        physics: Physics::new(),
    };

    let ground = RigidBodyDesc::new()
    .gravity_enabled(false)
    .build();
    let ground_h = engine.physics.bodies.insert(ground);
    let ground_s = ShapeHandle::new(Cuboid::new(glm::Vec3::new(5.0, 1.0, 5.0)));
    let ground_c = ColliderDesc::new(ground_s)
    .material(MaterialHandle::new(BasicMaterial::new(1.0, 0.0)))
    .build(BodyPartHandle(ground_h, 0));
    let ground_c_h = engine.physics.colliders.insert(ground_c);

    let mut player_phys = PhysicsObj::new();
    player_phys.info.status = physics::PhysicsStatus::Dynamic;
    player_phys.info.position = glm::Vec3::new(2.0, 4.0, 2.0);
    player_phys.info.kinematic_rotations = true;
    
    player_phys.c_info[0].shape = PhysicsShape::Cuboid(glm::Vec3::new(0.16, 0.72, 0.16));

    player_phys.c_info.push(physics::ColliderInfo::new());
    player_phys.c_info[1].shape = PhysicsShape::Cuboid(glm::Vec3::new(0.16, 0.01, 0.16));
    player_phys.c_info[1].sensor = true;
    player_phys.c_info[1].translation = glm::Vec3::new(0.0, -0.36, 0.0);
    
    engine.world.spawn((
        Transform::new(),
        Camera::new(10),
        CharacterController::new(),
        Player {},
        player_phys,
    ));

    let mut physics_obj = PhysicsObj::new();
    physics_obj.info.position = glm::Vec3::new(0.0, 1.5, 0.0);
    physics_obj.info.status = PhysicsStatus::Disabled;
    physics_obj.c_info[0].shape = PhysicsShape::Ball(0.25);

    let e1 = engine.world.spawn((
        *Transform::new().set_scale(glm::Vec3::new(0.25, 0.25, 0.25)),
        physics_obj,
        engine.resources.load_model("models/sphere.glb".into(), &mut engine.window.surface),
    ));

    let mut physics_obj2 = PhysicsObj::new();
    physics_obj2.info.position = glm::Vec3::new(-2.0, 1.5, 0.0);
    physics_obj2.info.status = PhysicsStatus::Disabled;
    physics_obj2.c_info[0].shape = PhysicsShape::Ball(0.25);

    engine.world.spawn((
        *Transform::new().set_scale(glm::Vec3::new(0.25, 0.25, 0.25)),
        physics_obj2,
        engine.resources.load_model("models/suzanne.glb".into(), &mut engine.window.surface),
    ));
    
    engine.world.spawn((
        *Transform::new().set_scale(glm::Vec3::new(5.0, 1.0, 5.0)),
        ground_h,
        engine.resources.load_model("models/cube.glb".into(), &mut engine.window.surface),
    ));
    
    let ser = serialize_entity(engine.world.entity(e1).unwrap());
    println!("{}", ser);
    /*let mut builder = hecs::EntityBuilder::new();
    
    deserialize_entity(ser.as_str(), &mut builder);
    engine.world.spawn(builder.build());
    deserialize_entity(ser.as_str(), &mut builder);
    engine.world.spawn(builder.build());
    deserialize_entity(ser.as_str(), &mut builder);
    engine.world.spawn(builder.build());*/

    //engine.world.remove_one::<ModelId>(e1);

    let forward = engine.console.get_var("forward");
    let back = engine.console.get_var("back");
    let left = engine.console.get_var("left");
    let right = engine.console.get_var("right");
    let speed = engine.console.get_var("speed");
    let jump = engine.console.get_var("jump");

    let mut start_t = Instant::now();
    #[allow(unused_variables)]
    let mut dt = 0.0;
    engine.console.add_var("host_timescale", 1.0);
    engine.console.add_var("sensitivity", 3.0);
    let sensitivity = engine.console.get_var("sensitivity");

    let mut ui = &mut ui::UIStack::new();

    engine.console.add_var("ui_stage", -1.0);
    let ui_stage = engine.console.get_var("ui_stage");

    engine.console.add_var("morph0", 1.0);
    engine.console.add_var("morph1", 0.0);
    engine.console.add_var("morph2", 0.0);
    engine.console.add_var("morph3", 0.0);

    {
        let morph0 = engine.console.get_var("morph0");
        let morph1 = engine.console.get_var("morph1");
        let morph2 = engine.console.get_var("morph2");
        let morph3 = engine.console.get_var("morph3");

        engine.console.add_cmd("morph".to_string(), Box::new(move |args| {
            if args.len() != 3 {
                println!("usage: morph <index> <increment>");
                return;
            }
            let which = args[1].parse::<usize>().unwrap();
            let inc = args[2].parse::<f32>().unwrap();

            let mut morphs: [f32; 4] = [morph0.get(), morph1.get(), morph2.get(), morph3.get()];
            morphs[which] = (-1.0_f32).max(1.0_f32.min(morphs[which] + inc));
            
            /*
            let mut total_other = 0.0;
            for i in 0..4 {
                if i != which {
                    total_other += morphs[i];
                }
            }
            let target_total_other = 1.0 - morphs[which];
            if total_other != 0.0 {
                for i in 0..4 {
                    if i != which {
                        morphs[i] *= target_total_other / total_other;
                    }
                }
            } else {
                for i in 0..4 {
                    if i != which {
                        morphs[i] += (target_total_other - total_other) / 3.0;
                    }
                }
            }
            */

            for i in 0..4 {
                println!("Morph {}: {:.1}", i, morphs[i]);
            }
            
            morph0.set(morphs[0]);
            morph1.set(morphs[1]);
            morph2.set(morphs[2]);
            morph3.set(morphs[3]);
        }))
    }

    engine.console.parse_exec("bind y \"morph 0 0.1\"".to_string());
    engine.console.parse_exec("bind h \"morph 0 -0.1\"".to_string());
    engine.console.parse_exec("bind u \"morph 1 0.1\"".to_string());
    engine.console.parse_exec("bind j \"morph 1 -0.1\"".to_string());
    engine.console.parse_exec("bind i \"morph 2 0.1\"".to_string());
    engine.console.parse_exec("bind k \"morph 2 -0.1\"".to_string());
    engine.console.parse_exec("bind o \"morph 3 0.1\"".to_string());
    engine.console.parse_exec("bind l \"morph 3 -0.1\"".to_string());

    engine.console.parse_exec("bind 1 \"morph0 1.0; morph1 0.0; morph2 0.0; morph3 0.0\"".to_string());
    engine.console.parse_exec("bind 2 \"morph0 1.0; morph1 0.4; morph2 0.0; morph3 0.0\"".to_string());
    engine.console.parse_exec("bind 3 \"morph0 1.0; morph1 0.2; morph2 0.0; morph3 0.0\"".to_string());
    engine.console.parse_exec("bind 4 \"morph0 1.0; morph1 1.0; morph2 0.0; morph3 0.0\"".to_string());
    engine.console.parse_exec("bind 5 \"morph0 0.6; morph1 1.0; morph2 0.0; morph3 0.0\"".to_string());
    engine.console.parse_exec("bind 6 \"morph0 0.3; morph1 1.0; morph2 0.0; morph3 0.0\"".to_string());
    engine.console.parse_exec("bind 7 \"morph0 0.0; morph1 1.0; morph2 0.0; morph3 0.0\"".to_string());
    
    while engine.window.input(&mut engine.console) {
        engine.physics.step();

        for (_id, (controller, camera, _player)) in &mut engine.world.query::<(&mut CharacterController, &mut Camera, &Player)>() {

            let yaw = glm::quat_angle_axis(-engine.window.mouse_dx * 0.001 * sensitivity.get(), &glm::Vec3::y_axis());
            let pitch = glm::quat_angle_axis(-engine.window.mouse_dy * 0.001 * sensitivity.get(), &glm::Vec3::x_axis());

            let rotation = yaw * camera.rotation * pitch;
            camera.rotation = rotation;
            
            let f = forward.get() - back.get();
            let r = right.get() - left.get();
            controller.move_dir = glm::quat_rotate_vec3(&rotation, &glm::Vec3::new(r, 0.0, -f));
            controller.jump_button = jump.get() != 0.0;
        }
        
        for (_id, (transform, controller, obj)) in &mut engine.world.query::<(&mut Transform, &mut CharacterController, &mut PhysicsObj)>() {
            controller.control(transform, obj, &mut engine.physics, dt);
        }

        for (_id, (transform, obj)) in &mut engine.world.query::<(&mut Transform, &mut PhysicsObj)>() {
            let handle = obj.build(&mut engine.physics);
            if let Some(body) = engine.physics.bodies.get(handle) {
                if let Some(part) = body.part(0) {
                    let part_pos = part.position().translation;
                    let part_rot = part.position().rotation;
                    transform.set_pos(part_pos.vector);
                    transform.set_ang(*part_rot.quaternion());
                }
            }
        }
        
        ui.clear();

        Div::new().padding(16, 16, 16, 16).build(ui, |ui| {

        });

        Div::new_if(ui_stage.get() >= 0.0).margin(0, 0, 0, 0).padding(0, 0, 0, 0).build(ui, |ui| {
            Div::new().background_color(0xFF000000).width(128).height(128).build(ui, |ui| {
    
            });
            Div::new_if(ui_stage.get() >= 1.0).background_color(0x0000FF00).build(ui, |ui| {
                Div::new_if(ui_stage.get() >= 2.0).background_color(0xFF000000).width(64).height(64).build(ui, |ui| {

                });
            });
            Div::new_if(ui_stage.get() >= 3.0).background_color(0x0000FF00).build(ui, |ui| {
                Div::new_if(ui_stage.get() >= 4.0).background_color(0xFFFFFF00).width(96).height(96).build(ui, |ui| {

                });
                Div::new_if(ui_stage.get() >= 5.0).background_color(0xFFFFFF00).width(32).height(32).build(ui, |ui| {

                });
                Div::new_if(ui_stage.get() >= 6.0).background_color(0xFFFFFF00).width(256).height(256).layout_direction(UIDirection::Row).build(ui, |ui| {
                    Div::new_if(ui_stage.get() >= 7.0).background_color(0xFF00FF00).padding(32, 32, 32, 32).build(ui, |ui| {

                    });
                    Div::new_if(ui_stage.get() >= 8.0).background_color(0xFF00FF00).width(64).height(64).build(ui, |ui| {

                    });
                });
            });
        });

        
        engine.gfx.morph0 = engine.console.get_var("morph0").get();
        engine.gfx.morph1 = engine.console.get_var("morph1").get();
        engine.gfx.morph2 = engine.console.get_var("morph2").get();
        engine.gfx.morph3 = engine.console.get_var("morph3").get();
        engine.gfx.render(&mut engine.window.surface, &mut engine.world, &mut engine.resources, &mut ui);
        
        let now = Instant::now();
        dt = (now - start_t).as_secs_f32() * engine.console.get_var("host_timescale").get();
        start_t = now;
    }
}