extern crate nalgebra as na;
extern crate nalgebra_glm as glm;
use serde::{Serialize, Deserialize};

use nphysics3d::object::{DefaultBodySet, DefaultColliderSet, DefaultBodyHandle, RigidBodyDesc, ColliderDesc, BodyPartHandle, DefaultColliderHandle, BodyStatus};
use ncollide3d::shape::{ShapeHandle, Ball, Cuboid, TriMesh, ConvexHull};
use nphysics3d::force_generator::DefaultForceGeneratorSet;
use nphysics3d::joint::DefaultJointConstraintSet;
use nphysics3d::world::{DefaultMechanicalWorld, DefaultGeometricalWorld};

use num_traits::identities::Zero;

pub struct Physics {
    pub mechanical_world: DefaultMechanicalWorld<f32>,
    pub geometrical_world: DefaultGeometricalWorld<f32>,
    pub bodies: DefaultBodySet<f32>,
    pub colliders: DefaultColliderSet<f32>,
    pub joint_constraints: DefaultJointConstraintSet<f32>,
    pub force_generators: DefaultForceGeneratorSet<f32>,
}

impl Physics {
    pub fn new() -> Self {
        let mechanical_world = DefaultMechanicalWorld::new(glm::Vec3::new(0.0, -9.81, 0.0));
        let geometrical_world = DefaultGeometricalWorld::new();
        let bodies = DefaultBodySet::new();
        let colliders = DefaultColliderSet::new();
        let joint_constraints = DefaultJointConstraintSet::new();
        let force_generators = DefaultForceGeneratorSet::new();

        Physics {
            mechanical_world,
            geometrical_world,
            bodies,
            colliders,
            joint_constraints,
            force_generators,
        }
    }
    pub fn step(&mut self) {
        self.mechanical_world.step(
            &mut self.geometrical_world,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.joint_constraints,
            &mut self.force_generators
        );
    }
}

#[derive(Serialize, Deserialize)]
pub enum PhysicsShape {
    Cuboid(glm::Vec3),
    Ball(f32),
}

#[derive(Serialize, Deserialize)]
pub enum PhysicsStatus {
    /// The body is disabled and ignored by the physics engine.
    Disabled,
    /// The body is static and thus cannot move.
    Static,
    /// The body is dynamic and thus can move and is subject to forces.
    Dynamic,
    /// The body is kinematic so its velocity is controlled by the user and it is not affected by forces and constraints.
    Kinematic,
}

#[derive(Serialize, Deserialize)]
pub struct RigidInfo {
    pub mass: f32,
    pub position: glm::Vec3,
    pub rotation: glm::Quat,
    pub status: PhysicsStatus,
    pub kinematic_rotations: bool,
    pub gravity_enabled: bool,
}

impl RigidInfo {
    pub fn new() -> Self {
        RigidInfo {
            mass: 1.0,
            position: glm::Vec3::zero(),
            rotation: glm::Quat::identity(),
            status: PhysicsStatus::Dynamic,
            kinematic_rotations: false,
            gravity_enabled: true,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct ColliderInfo {
    pub shape: PhysicsShape,
    pub translation: glm::Vec3,
    pub density: f32,
    pub sensor: bool,
    pub margin: f32,
}

impl ColliderInfo {
    pub fn new() -> Self {
        ColliderInfo {
            shape: PhysicsShape::Ball(1.0),
            translation: glm::Vec3::zero(),
            density: 1.0,
            sensor: false,
            margin: 0.01,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct PhysicsObj {
    pub info: RigidInfo,
    pub c_info: Vec<ColliderInfo>,
    #[serde(skip)]
    pub handle: Option<DefaultBodyHandle>,
    #[serde(skip)]
    pub c_handle: Vec<DefaultColliderHandle>,
}

impl PhysicsObj {
    pub fn new() -> Self {
        let mut c_info = Vec::new();
        c_info.push(ColliderInfo::new());
        PhysicsObj {
            info: RigidInfo::new(),
            c_info,
            handle: None,
            c_handle: Vec::new(),
        }
    }
    pub fn build(&mut self, phys: &mut Physics) -> DefaultBodyHandle {
        if let Some(handle) = self.handle {
            return handle;
        }
        let rigid_body = RigidBodyDesc::new()
        .mass(self.info.mass)
        .position(na::Isometry3::from_parts(self.info.position.into(), *na::UnitQuaternion::from_ref_unchecked(&self.info.rotation)))
        .status(match self.info.status {
            PhysicsStatus::Disabled => BodyStatus::Disabled,
            PhysicsStatus::Static => BodyStatus::Static,
            PhysicsStatus::Dynamic => BodyStatus::Dynamic,
            PhysicsStatus::Kinematic => BodyStatus::Kinematic,
            _ => BodyStatus::Static,
        })
        .kinematic_rotations(glm::TVec3::new(self.info.kinematic_rotations, self.info.kinematic_rotations, self.info.kinematic_rotations))
        .gravity_enabled(self.info.gravity_enabled)
        .build();
        let handle = phys.bodies.insert(rigid_body);
        self.handle = Some(handle);

        for (i, info) in self.c_info.iter().enumerate() {
            let mut margin = info.margin;
            let mut density = info.density;
            if info.sensor {
                margin = 0.0;
                density = 0.0;
            }
            let shape = match info.shape {
                PhysicsShape::Cuboid(v) => ShapeHandle::new(Cuboid::new(v - glm::Vec3::new(margin, margin, margin))),
                PhysicsShape::Ball(r) => ShapeHandle::new(Ball::new(r - margin)),
                _ => ShapeHandle::new(Ball::new(1.0 - margin))
            };
    
            let collider = ColliderDesc::new(shape)
            .density(density)
            .sensor(info.sensor)
            .translation(info.translation)
            .build(BodyPartHandle(handle, i));

            assert_eq!(self.c_handle.len(), i);
            self.c_handle.push(phys.colliders.insert(collider));
        }
        
        return handle;
    }
}