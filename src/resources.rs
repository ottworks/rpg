use crate::model::Model;
use std::collections::HashMap;
use slab::Slab;
use std::path::{PathBuf};
use luminance::context::GraphicsContext;
use std::fmt::Debug;
use serde::{Serialize, Deserialize};
use serde;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ModelId {
    #[serde(skip)]
    pub id: usize,
    pub path: PathBuf,
}

pub struct Resources {
    pub loaded_models: HashMap<PathBuf, ModelId>,
    pub model_slab: Slab<Model>,
}

impl Resources {
    pub fn new<C>(surface: &mut C) -> Self where C: GraphicsContext {
        let mut ret = Resources {
            loaded_models: HashMap::new(),
            model_slab: Slab::new(),
        };

        ret.load_model("models/error.glb".into(), surface);
    
        return ret;
    }
    pub fn load_model<C>(&mut self, path: PathBuf, surface: &mut C) -> ModelId where C: GraphicsContext {
        if let Some(id) = self.loaded_models.get(&path) {
            return id.to_owned();
        }
        let id = self.model_slab.insert(Model::load(&path, surface).unwrap());
        self.loaded_models.insert(path.clone(), ModelId {
            id,
            path: path.clone(),
        });
        return ModelId {
            id,
            path,
        };
    }
    pub fn get_mesh<C>(&mut self, model_id: &mut ModelId, surface: &mut C) -> &Model where C: GraphicsContext {
        if model_id.id == 0 {
            let new_id = self.load_model(model_id.path.clone(), surface);
            model_id.id = new_id.id;
        }
        return &self.model_slab[model_id.id];
    }
}