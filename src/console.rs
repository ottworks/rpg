use std::collections::HashMap;
use luminance_glfw::Key;
use std::vec::Vec;
use std::sync::Arc;
use std::sync::RwLock;
use crate::sharedptr::SharedPtr;

#[derive(Clone)]
pub struct ConVar {
    var: Arc<RwLock<f32>>,
}

impl ConVar {
    pub fn new(default: f32) -> Self {
        ConVar {
            var: Arc::new(RwLock::new(default)),
        }
    }
    pub fn get(&self) -> f32 {
        return *self.var.read().unwrap();
    }
    pub fn set(&self, f: f32) {
        if let Ok(mut value) = self.var.write() {
            *value = f;
        }
    }
}

fn find_key(s: &str) -> Option<Key> {
    match s {
        "q" => Some(Key::Q),
        "w" => Some(Key::W),
        "e" => Some(Key::E),
        "r" => Some(Key::R),
        "t" => Some(Key::T),
        "y" => Some(Key::Y),
        "u" => Some(Key::U),
        "i" => Some(Key::I),
        "o" => Some(Key::O),
        "p" => Some(Key::P),
        "a" => Some(Key::A),
        "s" => Some(Key::S),
        "d" => Some(Key::D),
        "f" => Some(Key::F),
        "g" => Some(Key::G),
        "h" => Some(Key::H),
        "j" => Some(Key::J),
        "k" => Some(Key::K),
        "l" => Some(Key::L),
        "z" => Some(Key::Z),
        "x" => Some(Key::X),
        "c" => Some(Key::C),
        "v" => Some(Key::V),
        "b" => Some(Key::B),
        "n" => Some(Key::N),
        "m" => Some(Key::M),
        "space" => Some(Key::Space),
        "alt" => Some(Key::LeftAlt),
        "ralt" => Some(Key::RightAlt),
        "shift" => Some(Key::LeftShift),
        "rshift" => Some(Key::RightShift),
        "ctrl" => Some(Key::LeftControl),
        "rctrl" => Some(Key::RightControl),
        "`" => Some(Key::GraveAccent),
        "1" => Some(Key::Num1),
        "2" => Some(Key::Num2),
        "3" => Some(Key::Num3),
        "4" => Some(Key::Num4),
        "5" => Some(Key::Num5),
        "6" => Some(Key::Num6),
        "7" => Some(Key::Num7),
        "8" => Some(Key::Num8),
        "9" => Some(Key::Num9),
        "0" => Some(Key::Num0),
        _ => None,
    }
}

pub struct Console {
    pub binds: SharedPtr<HashMap<Key, String>>,
    pub cmds: HashMap<String, Box<dyn Fn(Vec<&str>)>>,
    pub vars: HashMap<String, ConVar>,
}

impl Console {
    pub fn new() -> Self {
        let mut ret = Console {
            binds: SharedPtr::new(HashMap::new()),
            cmds: HashMap::new(),
            vars: HashMap::new(),
        };
        ret.binds.get_mut().insert(Key::W, "+forward".to_string());
        ret.binds.get_mut().insert(Key::A, "+left".to_string());
        ret.binds.get_mut().insert(Key::S, "+back".to_string());
        ret.binds.get_mut().insert(Key::D, "+right".to_string());
        ret.binds.get_mut().insert(Key::LeftShift, "+speed".to_string());
        ret.binds.get_mut().insert(Key::Space, "+jump".to_string());

        ret.add_input("forward");
        ret.add_input("back");
        ret.add_input("left");
        ret.add_input("right");
        ret.add_input("attack");
        ret.add_input("attack2");
        ret.add_input("attack3");
        ret.add_input("use");
        ret.add_input("speed");
        ret.add_input("jump");
        ret.add_input("duck");

        let binds = ret.binds.clone();
        ret.cmds.insert("bind".to_string(), Box::new(move |args| {
            if args.len() == 1 {
                println!("usage: bind <key> <command>");
                return;
            }
            if let Some(key) = find_key(args[1]) {
                if args.len() == 2 {
                    if let Some(bind) = binds.get().get(&key) {
                        println!("key {:?} is bound to: {}", key, bind);
                    } else {
                        println!("key {:?} is not bound", key);
                    }
                    return;
                }
                let command = args[2..].join(" ");
                binds.get_mut().insert(key, command);
            } else {
                println!("key not found: {}", args[1]);
            }
            
        }));
        return ret;
    }
    pub fn add_cmd(&mut self, name: String, fun: Box<dyn Fn(Vec<&str>)>) {
        self.cmds.insert(name, fun);
    }
    pub fn add_input(&mut self, name: &str) {
        self.vars.insert(name.to_owned(), ConVar::new(0.0));
        let var1 = self.get_var(name);
        let var2 = self.get_var(name);
        self.cmds.insert(format!("+{}", name), Box::new(move |_| {
            var1.set(1.0);
        }));
        self.cmds.insert(format!("-{}", name), Box::new(move |_| {
            var2.set(0.0);
        }));
        
    }
    pub fn add_var(&mut self, name: &str, default: f32) {
        self.vars.insert(name.to_owned(), ConVar::new(default));
    }
    pub fn get_var(&self, name: &str) -> ConVar {
        return self.vars.get(name).unwrap().clone();
    }
    pub fn exec(&self, args: Vec<&str>) {
        if args.len() == 0 {
            return;
        }
        if let Some(cmd) = self.cmds.get(args[0]) {
            cmd(args);
        } else if let Some(var) = self.vars.get(args[0]) {
            if args.len() == 1 {
                println!("\"{}\" = \"{}\"", args[0], var.get());
            } else {
                var.set(args[1].parse().unwrap());
            }
        } else {
            println!("Comamnd not found: {}", args[0]);
        }
    }
    pub fn parse_exec(&self, cmd: std::string::String) {
        println!("] {}", cmd);
        let mut cmds: Vec<Vec<&str>> = Vec::new();
        cmds.push(Vec::new());
        let mut quote = false;
        let mut cursor = 0;
        loop {
            let last_cmd = cmds.last_mut().unwrap();
            if quote {
                if let Some(next_quote) = cmd[cursor..].find("\"") {
                    if next_quote > 0 {
                        last_cmd.push(&cmd[cursor..cursor+next_quote]);
                    }
                    cursor += next_quote + 1;
                    quote = false;
                } else {
                    last_cmd.push(&cmd[cursor..]);
                    break;
                }
                
            } else {
                if let Some(next) = cmd[cursor..].chars().position(|c| match c {
                    '"' | ' ' | ';' => true,
                    _ => false,
                }) {
                    let found = &cmd[cursor+next..];
                    if found.starts_with('"') {
                        if next > 0 {
                            last_cmd.push(&cmd[cursor..cursor+next]);
                        }
                        quote = true;
                        cursor += next + 1;
                    } else if found.starts_with(" ") {
                        if next > 0 {
                            last_cmd.push(&cmd[cursor..cursor+next]);
                        }
                        cursor += next + 1;
                    } else if found.starts_with(";") {
                        if next > 0 {
                            last_cmd.push(&cmd[cursor..cursor+next]);
                        }
                        cmds.push(Vec::new());
                        cursor += next + 1;
                    }
                } else {
                    if cursor < cmd.len() {
                        last_cmd.push(&cmd[cursor..]);
                    }
                    break;
                }
            }
        }
        for args in cmds {
            self.exec(args);
        }
    }
}