in vec3 position;
in vec3 normal;

out vec3 v_normal;

uniform mat4 transform;

void main() {
    v_normal = normal;
    gl_Position = transform * vec4(position, 1.0);
}