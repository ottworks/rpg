use serde::{Serialize, Deserialize};
extern crate nalgebra_glm as glm;

#[derive(Serialize, Deserialize, Debug)]
pub struct Camera {
    pub priority: u32,
    pub rotation: glm::Quat,
}

impl Camera {
    pub fn new(priority: u32) -> Self {
        Camera {
            priority,
            rotation: glm::Quat::identity(),
        }
    }
}