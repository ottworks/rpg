in vec3 v_normal;
in float v_negative;

out vec4 frag_color;

void main() {
    vec3 obj_color = vec3(0.5, 0.5, 0.7);

    if (v_negative > 0.5f) {
    	obj_color = vec3(0.7, 0.7, 0.5);
    }

    vec3 light_dir = vec3(0.0, 1.0, 0.5);

    float kd = dot(v_normal, -light_dir);
    // Half Lambert shading so you can see the back of things
    kd = kd + 1;
    kd = kd / 2;
    // kd = kd * kd;

    frag_color = vec4(obj_color * kd, 1.0);
    frag_color.a = 0.3;
    //frag_color = v_normal;
}