use luminance::context::GraphicsContext;
use luminance::pipeline::PipelineState;
use luminance_glfw::{GlfwSurface, Surface};
use std::time::Instant;
use luminance_derive::{Semantics, Vertex, UniformInterface};
use luminance::tess::{TessSliceIndex};
use luminance::shader::program::{Program, Uniform};
use luminance::render_state::RenderState;
use luminance::framebuffer::Framebuffer;
use luminance::texture::{Dim2};
use luminance::linear::M44;
use hecs::{World, Entity};
use crate::transform::Transform;
use crate::resources::{Resources, ModelId};
use crate::camera::Camera;
use crate::ui;

extern crate nalgebra as na;
extern crate nalgebra_glm as glm;

const VS_STR: &str = include_str!("vs.glsl");
const FS_STR: &str = include_str!("fs.glsl");
const VS_UI_STR: &str = include_str!("vs_ui.glsl");
const FS_UI_STR: &str = include_str!("fs_ui.glsl");

const FOVY: f32 = std::f32::consts::PI / 2.0;
const Z_NEAR: f32 = 0.1;
const Z_FAR: f32 = 10.0;

struct RenderOp {
    model: usize,
    transform: Transform,
}

pub struct Graphics {
    start_t: Instant,
    back_buffer: Framebuffer<Dim2, (), ()>,
    program: Program<VertexSemantics, (), ShaderInterface>,
    program_ui: Program<VertexSemantics, (), ShaderInterface>,
    projection: glm::Mat4,
    view: glm::Mat4,
    pub morph0: f32,
    pub morph1: f32,
    pub morph2: f32,
    pub morph3: f32,
    font: rusttype::Font<'static>,
}

impl Graphics {
    pub fn new(surface: &mut GlfwSurface) -> Graphics {
        let start_t = Instant::now();
        let back_buffer = surface.back_buffer().unwrap();
        
        let program: Program<VertexSemantics, (), ShaderInterface> = Program::from_strings(None, VS_STR, None, FS_STR)
        .unwrap()
        .ignore_warnings();
        let program_ui: Program<VertexSemantics, (), ShaderInterface> = Program::from_strings(None, VS_UI_STR, None, FS_UI_STR)
        .unwrap()
        .ignore_warnings();
        
        let projection = glm::perspective(surface.width() as f32 / surface.height() as f32, FOVY, Z_NEAR, Z_FAR);
        let view = glm::Mat4::identity();

        let font_data = std::fs::read("fonts/Roboto/Roboto-Regular.ttf").unwrap();
        let font = rusttype::Font::try_from_vec(font_data).expect("error constructing font!");

        let morph0 = 0.0;
        let morph1 = 1.0;
        let morph2 = 0.0;
        let morph3 = 0.0;

        return Graphics {
            start_t,
            back_buffer,
            program,
            program_ui,
            projection,
            view,
            morph0,
            morph1,
            morph2,
            morph3,
            font,
        };
    }
    pub fn render(&mut self, surface: &mut GlfwSurface, world: &mut World, resources: &mut Resources, ui: &mut ui::UIStack) {

        // render
        let t = self.start_t.elapsed().as_millis() as f32 * 0.001;
        let color = [t.cos(), t.sin(), 0.5, 1.0];

        let mut top_entity: Option<Entity> = None;

        for (id, (_transform, camera)) in &mut world.query::<(&Transform, &Camera)>() {
            if let Some(ent) = top_entity {
                if camera.priority > world.get::<Camera>(ent).unwrap().priority {
                    top_entity = Some(id);
                }
            } else {
                top_entity = Some(id);
            }
        }

        if let Some(ent) = top_entity {
            if let Ok(transform) = &mut world.get_mut::<Transform>(ent) {
                self.view = *transform.matrix();
                if let Ok(camera) = &mut world.get_mut::<Camera>(ent) {
                    self.view = self.view * glm::quat_to_mat4(&camera.rotation);
                }
            }
        } else {
            println!("top camera: none");
        }
        

        let program = &self.program;
        let program_ui = &self.program_ui;
        self.view = self.view.try_inverse().unwrap();

        let mut operations = Vec::<RenderOp>::new();
        for (_id, (transform, model_id)) in &mut world.query::<(&mut Transform, &mut ModelId)>() {
            //let mesh = &resources.get_mesh(model_id, surface).tess;
            if model_id.id == 0 {
                let new_id = resources.load_model(model_id.path.clone(), surface);
                model_id.id = new_id.id;
            }
            operations.push(RenderOp {
                model: model_id.id,
                transform: *transform,
            })
        }

        let mut ui_verts: Vec<Vertex> = Vec::new();

        for (mut x, mut y, mut w, mut h, col) in &ui.rects {
            x /= surface.width() as f32;
            y /= surface.height() as f32;
            w /= surface.width() as f32;
            h /= surface.height() as f32;
            x -= 1.0;
            y -= 1.0;
            y = -y;
            
            let r = ((col & 0xFF000000) >> 24) as f32 / 255.0;
            let g = ((col & 0x00FF0000) >> 16) as f32 / 255.0;
            let b = ((col & 0x0000FF00) >> 8) as f32 / 255.0;

            ui_verts.push(Vertex::new( // Top left
                VertexPosition::new([x, y, 0.0]),
                VertexNormal::new([r, g, b]),
            ));
            ui_verts.push(Vertex::new( // Top right
                VertexPosition::new([x + w, y, 0.0]),
                VertexNormal::new([r, g, b]),
            ));
            ui_verts.push(Vertex::new( // Bottom right
                VertexPosition::new([x + w, y - h, 0.0]),
                VertexNormal::new([r, g, b]),
            ));
            ui_verts.push(Vertex::new( // Top left
                VertexPosition::new([x, y, 0.0]),
                VertexNormal::new([r, g, b]),
            ));
            ui_verts.push(Vertex::new( // Bottom right
                VertexPosition::new([x + w, y - h, 0.0]),
                VertexNormal::new([r, g, b]),
            ));
            ui_verts.push(Vertex::new( // Bottom left
                VertexPosition::new([x, y - h, 0.0]),
                VertexNormal::new([r, g, b]),
            ));
        }

        let ui_verts_tess = luminance::tess::TessBuilder::new(surface)
        .set_mode(luminance::tess::Mode::Triangle)
        .add_vertices(&ui_verts).build().unwrap();

        /*

        let scale = rusttype::Scale {
            x: 10.0,
            y: 10.0,
        };

        let v_metrics = self.font.v_metrics(scale);
        let offset = rusttype::point(0.0, v_metrics.ascent);
        let glyphs: Vec<_> = self.font.layout("Hello world!", scale, offset).collect();

        let glyphs_height = (v_metrics.ascent - v_metrics.descent).ceil() as u32;
        let glyphs_width = {
            let min_x = glyphs.first().map(|g| g.pixel_bounding_box().unwrap().min.x).unwrap();
            let max_x = glyphs.last().map(|g| g.pixel_bounding_box().unwrap().max.x).unwrap();
            (max_x - min_x) as u32
        };

        let glyph_verts: Vec<Vertex> = glyphs.iter().flat_map(|(uv_rect, screen_rect)| {
            let (screen_width, screen_height) = (surface.width() as f32, surface.height() as f32);
            let origin = rusttype::point(0.0, 0.0);
            let gl_rect = rusttype::Rect {
                min: origin + rusttype::vector(
                    screen_rect.min.x as f32 / screen_width - 0.5, 
                    1.0 - screen_rect.min.y as f32 / screen_height - 0.5,
                ) * 2.0,
                max: origin + rusttype::vector(
                    screen_rect.max.x as f32 / screen_width - 0.5,
                    1.0 - screen_rect.max.y as f32 / screen_height - 0.5,
                ) * 2.0,
            };
            vec![
                Vertex::new(
                    VertexPosition::new([])
                    VertexNormal::new([r, g, b]),
                )
            ]
        }).collect();

        */

        surface.pipeline_builder().pipeline(
            &self.back_buffer, 
            &PipelineState::default().set_clear_color(color), 
            |_, mut shd_gate| {
                shd_gate.shade(program, |iface, mut rdr_gate| {
                    iface.projection.update(self.projection.into());
                    iface.view.update(self.view.into());
                    iface.morph0.update(self.morph0.into());
                    iface.morph1.update(self.morph1.into());
                    iface.morph2.update(self.morph2.into());
                    iface.morph3.update(self.morph3.into());

                    for operation in &mut operations {
                        iface.transform.update((operation.transform.matrix() * glm::Mat4::new(
                            -1.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, -1.0, 0.0,
                            0.0, -1.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 1.0,
                        )).into()); // I don't know
                        rdr_gate.render(&RenderState::default(), |mut tess_gate| {
                            tess_gate.render(resources.model_slab[operation.model].tess.slice(..));
                        });
                    }
                });
                shd_gate.shade(program_ui, |iface, mut rdr_gate| {
                    iface.transform.update(glm::Mat4::identity().into());
                    rdr_gate.render(&RenderState::default(), |mut tess_gate| {
                        tess_gate.render(ui_verts_tess.slice(..));
                    });
                });
            },
        );

        surface.swap_buffers();
    }
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, Semantics)]
pub enum VertexSemantics {
    #[sem(name = "position", repr = "[f32; 3]", wrapper = "VertexPosition")]
    Position,
    #[sem(name = "normal", repr = "[f32; 3]", wrapper = "VertexNormal")]
    Normal,
}

#[derive(Clone, Copy, Debug, Vertex)]
#[vertex(sem = "VertexSemantics")]
pub struct Vertex {
    pub position: VertexPosition,
    pub normal: VertexNormal,
}

pub type VertexIndex = u32;

#[derive(Debug, UniformInterface)]
struct ShaderInterface {
    #[uniform(unbound)]
    projection: Uniform<M44>,
    #[uniform(unbound)]
    view: Uniform<M44>,
    #[uniform(unbound)]
    transform: Uniform<M44>,
    #[uniform(unbound)]
    morph0: Uniform<f32>,
    #[uniform(unbound)]
    morph1: Uniform<f32>,
    #[uniform(unbound)]
    morph2: Uniform<f32>,
    #[uniform(unbound)]
    morph3: Uniform<f32>,
}
