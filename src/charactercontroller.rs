use nalgebra_glm as glm;
use crate::transform::Transform;
use crate::physics::{PhysicsObj, Physics};
use nphysics3d::algebra::Velocity3;
use num_traits::identities::Zero;

pub struct CharacterController {
    pub velocity: glm::Vec3,
    pub move_dir: glm::Vec3,
    pub on_ground: bool,
    pub jump_button: bool,
}

impl CharacterController {
    pub fn new() -> Self {
        CharacterController {
            velocity: glm::Vec3::new(0.0, 0.0, 0.0),
            move_dir: glm::Vec3::new(0.0, 0.0, 0.0),
            on_ground: false,
            jump_button: false,
        }
    }
    pub fn control(&mut self, _transform: &mut Transform, obj: &mut PhysicsObj, physics: &mut Physics, dt: f32) {
        if let Some(handle) = obj.handle {
            if let Some(body) = physics.bodies.rigid_body_mut(handle) {
                self.on_ground = false;
                for i in 0..obj.c_info.len() {
                    let handle = obj.c_handle[i];
                    for (h1, _c1, h2, _c2, _, _proximity) in physics.geometrical_world.proximity_pairs(&physics.colliders, true) {
                        if handle == h1 && obj.c_handle.iter().find(|x| **x == h2) == None || handle == h2 && obj.c_handle.iter().find(|x| **x == h1) == None {
                            self.on_ground = true;
                        }
                    }
                }
                self.velocity = body.velocity().linear;

                if self.on_ground && self.jump_button {
                    self.on_ground = false;
                    self.velocity.y = if self.velocity.y < 0.0 {
                        3.0
                    } else {
                        self.velocity.y + 3.0
                    };
                }

                self.move_dir.y = 0.0;
                if self.move_dir.norm_squared() != 0.0 {
                    self.move_dir = glm::normalize(&self.move_dir);
                    if self.on_ground {
                        self.accelerate(10.0, 2.0, dt);
                    } else {
                        self.accelerate(10.0, 1.0, dt);
                    }
                }

                body.set_velocity(Velocity3::new(self.velocity, glm::Vec3::zero()))
            }
        }
    }
    pub fn accelerate(&mut self, acceleration: f32, max_velocity: f32, dt: f32) {
        let proj_vel = glm::dot(&self.velocity, &self.move_dir);
        let mut accel = acceleration * dt;
        if proj_vel + accel > max_velocity {
            accel = max_velocity - proj_vel;
        }
        if accel < 0.0 {
            accel = 0.0;
        }
        self.velocity += self.move_dir * accel;
    }
}