The main bit of code for this assignment is in src/vs.glsl. Interface stuff is in src/main.rs. There are four morph coefficients to control.

Build instructions (tested on Windows, may work on other platforms?):
Install Rust from https://www.rust-lang.org/
In this folder do: cargo run

Controls:
Hold right click to look
esc: exit
WASD to move
Space to jump
y: Morph 0 += 0.1
h: Morph 0 -= 0.1
u: Morph 1 += 0.1
j: Morph 1 -= 0.1
i: Morph 2 += 0.1
k: Morph 2 -= 0.1
o: Morph 3 += 0.1
l: Morph 3 -= 0.1
1: Omni
2: Cardioid
3: Subcardioid
4: Supercardioid
6: Hypercardioid
7: Bidirectional